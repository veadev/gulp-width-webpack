'use strict'
import fs from 'fs'
import path from 'path'
import { deleteAsync } from 'del'
import mode from 'gulp-mode'
import { config } from 'dotenv'
import gulp from 'gulp'
import pug from 'gulp-pug'
import sync from 'browser-sync'
import gulpSass from 'gulp-sass'
import dartSass from 'sass'
import sourcemaps from 'gulp-sourcemaps'
import concat from 'gulp-concat'
import autoprefixer from 'gulp-autoprefixer'
import groupCssMediaQueries from 'gulp-group-css-media-queries'
import csso from 'gulp-csso'
import rename from 'gulp-rename'
import webpackStream from 'webpack-stream'
import webpackConfig from './webpack.config.js'
import webpack from 'webpack'
import ttf2woff from 'gulp-ttf2woff'
import ttf2woff2 from 'gulp-ttf2woff2'
import svgstore from 'gulp-svgstore'
import gulpFTP from 'vinyl-ftp'
import imagemin from 'gulp-imagemin'
import imageminGifsicle from 'imagemin-gifsicle'
import imageminMozjpeg from 'imagemin-mozjpeg'
import imageminOptipng from 'imagemin-optipng'
import imageminSvgo from 'imagemin-svgo'
import imageminWebp from 'imagemin-webp'
import gulpRename from 'gulp-rename'

const modeGulp = mode({
  modes: ['production', 'development'],
  default: 'development',
  verbose: false,
})

// ENV config
config()
const env = process.env

// Clean dist
const cleanTask = () => {
  return deleteAsync(['dist'])
}

// Json data generation
const data = {}
const dir = './src/components/'
export const jsonGenerateTask = (cb) => {
  try {
    const modules = fs.readdirSync(dir)
    modules.forEach((item) => {
      const module = path.join(dir, item)
      if (!fs.lstatSync(module).isDirectory()) return
      const jsons = fs.readdirSync(module).filter((item) => path.extname(item) === '.json')
      jsons.forEach((json) => {
        const name = path.basename(json, path.extname(json))
        const file = path.join(dir, item, json)
        data[name] = JSON.parse(fs.readFileSync(file))
      })
    })
  } catch (e) {
    console.log(e)
  }
  if (cb) {
    return cb()
  }
}
data['version'] = new Date().getTime()
jsonGenerateTask()

// Pug
export const pugTask = () => {
  console.log(modeGulp.production())
  return gulp
    .src('src/pages/*.pug')
    .pipe(
      pug({
        basedir: './',
        locals: {
          data: data,
        },
        pretty: modeGulp.production()
      })
    )
    .pipe(gulp.dest('dist'))
    .pipe(modeGulp.development(sync.stream()))
}

// Sass
const sass = gulpSass(dartSass)
export const sassTask = () => {
  return gulp
    .src([
      'src/static/scss/vars.scss',
      'src/static/scss/fonts.scss',
      'src/static/scss/common.scss',
      'src/components/**/*.scss',
    ])
    .pipe(modeGulp.development(sourcemaps.init()))
    .pipe(concat('style.scss'))
    .pipe(sass())
    .pipe(
      modeGulp.production(
        autoprefixer({
          overrideBrowserslist: ['last 4 versions'],
          cascade: false,
          grid: true,
        })
      )
    )
    .pipe(modeGulp.production(groupCssMediaQueries()))
    .pipe(modeGulp.production(gulp.dest('dist/css')))
    .pipe(modeGulp.production(csso()))
    .pipe(
      rename(function (path) {
        path.basename += '.min'
      })
    )
    .pipe(modeGulp.development(sourcemaps.write('.')))
    .pipe(gulp.dest('dist/css'))
    .pipe(modeGulp.development(sync.stream()))
}

// Javascript
export const jsTask = () => {
  return gulp
    .src(['src/static/js/app.js'])
    .pipe(webpackStream(webpackConfig), webpack)
    .pipe(gulp.dest('dist/js'))
    .pipe(modeGulp.development(sync.stream()))
}

// Json copy
export const jsonCopyTask = () => {
  return gulp.src('src/pages/**/*.json').pipe(gulp.dest('dist'))
}

// Video copy
export const videoCopyTask = () => {
  return gulp.src('src/static/video/*.*').pipe(gulp.dest('dist/video'))
}

// Js static copy
export const jsCopyTask = () => {
  return gulp.src('src/static/js/static/*.js').pipe(gulp.dest('dist/js'))
}

// Fonts convert & copy
export const fontsTask = () => {
  gulp.src('src/static/fonts/**/*.ttf').pipe(ttf2woff()).pipe(gulp.dest('src/static/fonts'))

  return gulp.src('src/static/fonts/**/*.ttf').pipe(ttf2woff2()).pipe(gulp.dest('src/static/fonts'))
}
export const fontsCopyTask = () => {
  return gulp.src('src/static/fonts/**/*.{woff,woff2}').pipe(gulp.dest('dist/fonts'))
}

// Svg icon sprite
export const svgstoreTask = () => {
  return gulp
    .src('src/static/img/icons/*.svg')
    .pipe(modeGulp.production(imagemin([imageminSvgo()])))
    .pipe(svgstore())
    .pipe(gulp.dest('dist/img'))
}

// Images optimization
export const imgOptim = () => {
  return gulp
    .src(['src/static/img/**/*.{jpg,jpeg,png,gif,svg}', '!src/static/img/icons/*.svg'])
    .pipe(
      modeGulp.production(
        imagemin([
          imageminGifsicle({ interlaced: true }),
          imageminMozjpeg({ quality: 85, progressive: true }),
          imageminOptipng({ optimizationLevel: 6 }),
          imageminSvgo('svgo', {
            plugins: [{ removeViewBox: false }, { addClassesToSVGElement: '' }],
          }),
        ])
      )
    )
    .pipe(gulp.dest('dist/img'))
}

// Images webp convert
export const imgWebp = () => {
  return gulp
    .src('src/static/img/*.{jpg,jpeg,png,gif}')
    .pipe(imagemin([imageminWebp({ quality: 85 })]))
    .pipe(
      gulpRename(function (path) {
        path.extname += '.webp'
      })
    )
    .pipe(gulp.dest('dist/img'))
}

// Ftp
export const ftpTask = () => {
  const ftpConnect = gulpFTP.create({
    host: env.FTP_HOST,
    user: env.FTP_USER,
    password: env.FTP_PASSWORD,
    parallel: 5,
  })
  return gulp.src('./dist/**/*.*').pipe(ftpConnect.dest('/' + env.FTP_DIRECTORY))
}

// Server
export const serverTask = () => {
  sync.init({
    notify: false,
    server: {
      baseDir: 'dist',
    },
  })
}

// Watching
export const watchTask = () => {
  gulp.watch('src/**/*.pug', gulp.series(pugTask))
  gulp.watch('src/components/**/*.json', gulp.series(jsonGenerateTask, pugTask))
  gulp.watch(['src/static/scss/**/*.scss', 'src/components/**/*.scss'], gulp.series(sassTask))
  gulp.watch('src/static/js/**/*.js', gulp.series(jsTask, jsCopyTask))
  gulp.watch('src/static/img/icons/*.svg', gulp.series(svgstoreTask))
  gulp.watch('src/static/fonts/**/*.{woff,woff2}', gulp.series(fontsCopyTask))
  gulp.watch(
    ['src/static/img/**/*.{jpg,jpeg,png,gif,svg}', '!src/static/img/icons/*.svg'],
    gulp.series(imgOptim)
  )
}

// Gulp task default
let defaultTask = ''
if (modeGulp.development()) {
  webpackConfig.mode = 'development'
  webpackConfig.devtool = 'source-map'
  defaultTask = gulp.series(
    cleanTask,
    gulp.parallel(
      sassTask,
      jsTask,
      fontsCopyTask,
      imgOptim,
      jsonCopyTask,
      videoCopyTask,
      jsCopyTask,
      pugTask,
      svgstoreTask
    ),
    gulp.parallel(watchTask, serverTask)
  )
} else {
  webpackConfig.mode = 'production'
  defaultTask = gulp.series(
    cleanTask,
    gulp.parallel(
      sassTask,
      jsTask,
      fontsCopyTask,
      jsonCopyTask,
      videoCopyTask,
      pugTask,
      jsCopyTask,
      svgstoreTask
    ),
    gulp.parallel(imgOptim, imgWebp)
  )
}
export default defaultTask
